const hamburger = document.querySelector('.hamburger');
const navlinks = document.querySelector('.nav-links');
const links = document.querySelectorAll('.nav-links li');
const atags = document.querySelector('.nav-links li a');
const contactbutton = document.querySelector('.contact-btn')
// const project = document.querySelector('.recent-projects');
const scrolldown = document.querySelector('.scrolldown');

hamburger.addEventListener("click",()=>{
    navlinks.classList.toggle("open");
    links.forEach(link=>{
        link.classList.toggle("fade");
    });
    scrolldown.classList.toggle("off");
    contactbutton.classList.toggle("disable");
});

atags.addEventListener("click",()=>{
    navlinks.classList.toggle("close");
});

// ============ Fade In Scroll Animation ================

// $(document).on("scroll", function() {
//     var pageTop = $(document).scrollTop();
//     var pageBottom = pageTop + $(window).height();
//     var tags = $(".hideme");
    
//     for (var i = 0; i < tags.length; i++) {
//     var tag = tags[i];
    
//     if ($(tag).position().top < pageBottom) {
//     $(tag).addClass("visible");
//     } else {
//     $(tag).removeClass("visible");
//     }
//     }
//     });




